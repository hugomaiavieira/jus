# Jusbrasil

Even not doing Python for a long time and never done Golang, I've decided to use those in the project to see if I would like and to show to you and myself that I can learn and start doing it in a short time. Doing this challenge was a great experience for that purpose.

One thing to notice about the git history is that in the first stage doing the challenge, I was learning and experimenting a lot of things. So there is no git history about it, only about the refactorings.

## archtecture and flow

The chart bellow describes the archtecture:


```
                             +----------+
                             |          |
             +---------------+ RabbitMQ |
             |               |          |
             |               +------+-+-+
             |                      | |
        +----+----+                 | |        +-----------+
        |         |                 | |        |           |
        |         |                 | +--------+ Crawler 1 |
+------>+   API   +---------+       |          |           |
        |         |         |       |          +-----------+
        |         |  Queue  |       |
        +----+----+ Service |       |          +-----------+
             |    |         |       |          |           |
             |    +---------+       +----------+ Crawler n |
             |                                 |           |
             |                                 +-----------+
        +----+----+
        |         |
        |   DB    |
        | (Redis) |
        |         |
        +---------+
```

The client sends a POST request to API at `/lawsuit/search` with a json in the body with a `number` attribute (the lawsuit number), like:

``` json
{
    "number": "08219015120188120001"
}
```

Using `curl`, for example:

``` bash
curl -X POST -H "Content-Type: application/json" -d '{ "number": "08219015120188120001" }' localhost:5000/lawsuit/search
```

The API receives it, save into the DB and publish an event in the RabbitMQ's "input" queue.

The Crawler, that is listening to the "input" queue, then receives the message with the input data, get the information from the courts websites and publish in the "result" queue.

The API Queue Service, that is listening to the "result" queue, then receives a message with the result and save it into the database.

The client now can retreive the result sending a GET request to `/lawsuit/search/<lawsuit number>`. Using `curl`, for example:

``` bash
curl localhost:5000/lawsuit/search/08219015120188120001
```

## Setup

First, setup the git submodules with the API and crawler projects:

``` bash
git submodule init
git submodule update
```

After that, you will only need to install [docker](https://docs.docker.com/install) and [docker-compose](https://docs.docker.com/compose/install). Then you will need to build the the apps images:

    docker-compose build

Finally, you will be able to run the apps and tests.

## Running the apps

To run the apps, you only need to run.

    docker-compose up

This will bring up the API (available at http://localhost:5000), the Queue Service, and the Crawler, as well as the Redis and RabbitMQ.

You can also bring more than one instance of the crawler, for example:
    
    docker-compose up --scale crawler=3

This will bring up 3 instances of the crawler.

## Tests

Having the apps running, you can use `docker-compose exec` for the following commands. Otherwise, you will need to use `docker-compose run --rm`.

### Api and Queue Service

    docker-compose exec api-server pipenv run python -m pytest

### Crawler

    docker-compose exec crawler go test ./...
